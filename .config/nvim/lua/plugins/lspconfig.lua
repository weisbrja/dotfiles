return {
	"neovim/nvim-lspconfig",
	lazy = false,
	dependencies = {
		{
			"williamboman/mason.nvim",
			config = true,
		},
		"williamboman/mason-lspconfig",
		"hrsh7th/nvim-cmp",
		"hrsh7th/cmp-nvim-lsp",
		-- specific config plugins
		"pest-parser/pest.vim",
		"simrat39/rust-tools.nvim",
	},
	config = function()
		local capabilities = require("cmp_nvim_lsp").default_capabilities()
		capabilities.textDocument.completion.completionItem.snippetSupport = true
		-- capabilities.textDocument.documentHighlight = true
		-- vim.lsp.handlers["textDocument/publishDiagnostics"] = vim.lsp.with(vim.lsp.diagnostic.on_publish_diagnostics, { underline = false })

		local function on_attach(_, bufnr)
			local opts = { buffer = bufnr, remap = false }

			vim.keymap.set("n", "K", vim.lsp.buf.hover, opts)
			vim.keymap.set("n", "gd", function()
				require("telescope.builtin").lsp_definitions()
			end, opts)
			vim.keymap.set("n", "gt", function()
				require("telescope.builtin").lsp_type_definitions()
			end, opts)
			vim.keymap.set("n", "gi", function()
				require("telescope.builtin").lsp_implementations()
			end, opts)
			vim.keymap.set("n", "gr", function()
				require("telescope.builtin").lsp_references()
			end, opts)
			vim.keymap.set("n", "<Leader>x", vim.diagnostic.setqflist, opts)
			vim.keymap.set("n", "<Leader>v", vim.diagnostic.open_float, opts)
			vim.keymap.set("n", "<Leader>a", vim.lsp.buf.code_action, opts)
			vim.keymap.set("n", "<Leader>r", vim.lsp.buf.rename, opts)
			vim.keymap.set("i", "<C-s>", vim.lsp.buf.signature_help, opts)
		end

		-- lsp config not handled by mason
		local lspconfig = require("lspconfig")
		lspconfig.hls.setup({ on_attach = on_attach, capabilities = capabilities })

		local mason_lspconfig = require("mason-lspconfig")
		mason_lspconfig.setup_handlers({
			function(server)
				lspconfig[server].setup({ on_attach = on_attach, capabilities = capabilities })
			end,

			["pylsp"] = function()
				lspconfig.pylsp.setup({
					settings = {
						pylsp = {
							plugins = { pycodestyle = { maxLineLength = 88 } },
						},
					},
					on_attach = on_attach,
					capabilities = capabilities,
				})
			end,

			["cssls"] = function()
				lspconfig.cssls.setup({
					settings = {
						css = {
							lint = { unknownAtRules = "ignore" },
						},
					},
					on_attach = on_attach,
					capabilities = capabilities,
				})
			end,

			["lua_ls"] = function()
				lspconfig.lua_ls.setup({
					on_attach = on_attach,
					capabilities = capabilities,
					settings = {
						Lua = {
							runtime = { version = "LuaJIT" },
							workspace = { library = vim.api.nvim_get_runtime_file("", true) },
						},
					},
				})
			end,

			["pest_ls"] = function()
				require("pest-vim").setup({})
			end,

			["rust_analyzer"] = function()
				require("rust-tools").setup({
					server = {
						on_attach = on_attach,
						capabilities = capabilities,
						settings = {
							["rust-analyzer"] = {
								imports = {
									granularity = {
										group = "module",
									},
									prefix = "self",
								},
								completion = { callable = { snippets = "none" } }, -- FIXME: terrible workaround
								cargo = { buildScripts = { enable = true } },
								procMacro = { enable = true },
							},
						},
					},
				})
			end,
		})
	end,
}
