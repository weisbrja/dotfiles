return {
	"saecki/crates.nvim",
	lazy = true,
	event = "BufRead Cargo.toml",
	config = true,
}
