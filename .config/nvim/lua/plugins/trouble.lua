-- TODO: quickfix lists and scratch buffers seem very interesting!
-- future me, please have a closer look at this plugin.
return {
	"folke/trouble.nvim",
	lazy = true,
	enabled = false,
	cmd = "Trouble",
	dependencies = { "nvim-tree/nvim-web-devicons" },
	opts = {
		auto_close = true,
		follow = true,
	},
	keys = {
		{ "<Leader>xx", "<Cmd>Trouble diagnostics toggle<CR>" },
		{ "<Leader>xX", "<Cmd>Trouble diagnostics toggle filter.buf=0<CR>" },
		{ "<Leader>xl", "<Cmd>Trouble loclist toggle<CR>" },
		{ "<Leader>xq", "<Cmd>Trouble qflist toggle<CR>" },
	},
}
