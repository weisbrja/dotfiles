return {
	"NvChad/nvim-colorizer.lua",
	opts = {
		user_default_options = {
			mode = "virtualtext",
			tailwind = true,
			rgb_fn = true,
			always_update = true,
		},
	},
}
