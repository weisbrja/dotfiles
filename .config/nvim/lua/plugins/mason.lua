return {
	"whoissethdaniel/mason-tool-installer.nvim",
	cmd = { "MasonToolsInstall", "MasonToolsUpdate", "MasonToolsClean" },
	lazy = true,
	opts = {
		ensure_installed = {
			-- lsps
			"bash-language-server",
			"clangd",
			"css-lsp",
			"flake8",
			"html-lsp",
			"jdtls",
			"json-lsp",
			"lua-language-server",
			"ocaml-lsp",
			"pest-language-server",
			"python-lsp-server",
			"rust-analyzer",
			"tailwindcss-language-server",
			"texlab",
			"typescript-language-server",
			"zls",
			-- formatters
			"black",
			"prettier",
			"stylua",
		},
		integrations = {
			["mason-lspconfig"] = false,
			["mason-null-ls"] = false,
			["mason-nvim-dap"] = false,
		},
	},
}
