-- lazy mappings here because we require noremap
vim.keymap.set({ "i", "n" }, "<M-n>", function()
	require("copilot.suggestion").next()
end, { remap = false })
vim.keymap.set({ "i", "n" }, "<M-p>", function()
	require("copilot.suggestion").prev()
end, { remap = false })
vim.keymap.set({ "i", "n" }, "<M-CR>", function()
	require("copilot.suggestion").accept()
end, { remap = false })

return {
	"zbirenbaum/copilot.lua",
	lazy = true,
	cmd = "Copilot",
	opts = {
		panel = { enabled = false },
		suggestion = {
			auto_trigger = false,
			keymap = {
				accept = false,
				accept_word = false,
				accept_line = false,
				next = false,
				prev = false,
				dismiss = false,
			},
		},
	},
}
