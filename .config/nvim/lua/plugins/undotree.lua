return {
	"mbbill/undotree",
	lazy = true,
	keys = { { "<Leader>u", vim.cmd.UndotreeToggle } },
	cmd = "UndotreeToggle",
}
