local function oilfd(opts)
	require("telescope.builtin").find_files({
		cwd = require("oil").get_current_dir(),
		find_command = {
			"fd",
			"--type",
			opts.type,
			"--color",
			"never",
			"--exclude",
			".git/",
		},
		hidden = true,
		no_ignore_parent = true,
		no_ignore = opts.no_ignore,
	})
end

return {
	{
		"jamestrew/telescope.nvim",
		branch = "preview-opt-inheritance",
		-- "nvim-telescope/telescope.nvim",
		dependencies = {
			"nvim-lua/plenary.nvim",
			"nvim-telescope/telescope-ui-select.nvim",
			{ "nvim-telescope/telescope-fzf-native.nvim", build = "make" },
		},
		lazy = true,
		keys = {
			{
				"<Leader>t",
				function()
					require("telescope.builtin").resume()
				end,
			},
			{
				"<Leader>f",
				function()
					oilfd({ type = "file" })
				end,
			},
			{
				"<Leader>F",
				function()
					oilfd({ type = "file", no_ignore = true })
				end,
			},
			{
				"<Leader>d",
				function()
					oilfd({ type = "directory" })
				end,
			},
			{
				"<Leader>D",
				function()
					oilfd({ type = "directory", no_ignore = true })
				end,
			},
			{
				"<Leader>/",
				function()
					require("telescope.builtin").live_grep()
				end,
			},
			{
				"<Leader>b",
				function()
					require("telescope.builtin").buffers()
				end,
			},
			"<Leader>H",
		},
		opts = {
			defaults = {
				vimgrep_arguments = {
					"rg",
					"--color=never",
					"--no-heading",
					"--with-filename",
					"--line-number",
					"--column",
					"--smart-case",
					-- custom from here
					"--hidden",
					"--no-ignore",
					"--trim",
				},
				sorting_strategy = "ascending",
				layout_strategy = "vertical",
				layout_config = {
					prompt_position = "top",
					preview_cutoff = 15,
				},
				preview = true,
				border = true,
			},
			pickers = {
				live_grep = {
					layout_strategy = "vertical",
					layout_config = {
						prompt_position = "top",
						preview_cutoff = 15,
					},
					preview = true,
				},
				find_files = {
					layout_strategy = "center",
					preview = false,
				},
				buffers = {
					layout_strategy = "center",
					preview = false,
				},
			},
		},
		config = function(_, opts)
			local telescope = require("telescope")
			telescope.setup(opts)
			telescope.load_extension("fzf")
			telescope.load_extension("ui-select")

			-- telescope.load_extension("harpoon")
			local conf = require("telescope.config").values
			local function toggle_telescope(harpoon_files)
				local file_paths = {}
				for _, item in ipairs(harpoon_files.items) do
					table.insert(file_paths, item.value)
				end

				-- FIXME: global preview option still can't be overridden here
				require("telescope.pickers")
					.new({
						preview = false,
					}, {
						preview = false,
						prompt_title = "Harpoon",
						layout_strategy = "center",
						finder = require("telescope.finders").new_table({
							results = file_paths,
							-- I tried this, but it didn't change anything and I didn't need it before.
							-- entry_maker = require("telescope.make_entry").gen_from_file({ preview = false }),
						}),
						previewer = conf.file_previewer({}),
						sorter = conf.generic_sorter({}),
					})
					:find()
			end
			vim.keymap.set("n", "<Leader>H", function()
				toggle_telescope(require("harpoon"):list())
			end)

			vim.api.nvim_set_hl(0, "TelescopeNormal", { link = "CursorLine" })
			vim.api.nvim_set_hl(0, "TelescopePromptCounter", { link = "StatusLineNC" })
		end,
	},
}
