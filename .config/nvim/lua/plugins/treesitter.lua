return {
	"nvim-treesitter/nvim-treesitter",
	build = function()
		require("nvim-treesitter.install").update()
	end,
	-- lazy = vim.fn.argc(-1) == 0,
	lazy = true,
	event = "VeryLazy",
	opts = {
		ensure_installed = "all",
		sync_install = false,
		auto_install = false,
		indent = { enable = true },
		highlight = {
			enable = true,
			additional_vim_regex_highlighting = false,
		},
	},
	config = function(_, opts)
		require("nvim-treesitter.configs").setup(opts)
	end,
}
