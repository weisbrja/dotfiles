#!/bin/zsh

### syntax highlighting
source '/usr/share/zsh/plugins/fast-syntax-highlighting/fast-syntax-highlighting.plugin.zsh'
source '/usr/share/zsh/plugins/zsh-fzf-plugin/fzf.plugin.zsh'

### zoxide
eval "$(zoxide init zsh)"

### settings

# history settings
setopt hist_ignore_all_dups hist_save_no_dups hist_reduce_blanks share_history

# enable comments
setopt interactive_comments

### prompt
autoload -Uz colors && colors
PS1='%B%F{yellow}%n%b%f@%B%F{magenta}%M %F{blue}%~ %(?.%F{green}.%F{red})$%b%f '
PS2=''

### vim mode

# enable vim mode
bindkey -v
export KEYTIMEOUT=1

# cursor shapes for different vim modes
cursor_block='\e[1 q'
cursor_beam='\e[5 q'
function zle-keymap-select {
	case "$KEYMAP" in
		vicmd) printf "$cursor_block" ;;
		viins|main) printf "$cursor_beam" ;;
	esac
}
zle -N zle-keymap-select

function precmd {
	# set terminal title to current working directory
	print -Pn '\e]0;%~\a'
}

function preexec {
	# set terminal title to current command
	printf "\e]0;$1\a"

	# reset cursor shape
	printf "$cursor_beam"
}

zle-line-init() {
	# reset cursor shape
	printf "$cursor_beam"
}
zle -N zle-line-init

### completion

# initialize completion
zmodload zsh/complist
autoload -Uz compinit && compinit -d "${XDG_CACHE_HOME:-$HOME/.cache}/zsh/zcompdump"

# completion settings
setopt auto_menu auto_list list_ambiguous complete_in_word extended_glob glob_dots correct

# define completers
zstyle ':completion:*' completer _extensions _complete _approximate

zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path "${XDG_CACHE_HOME:-$HOME/.cache}/zsh/zcompcache"

# TODO: figure out what this does
zstyle ':completion:*' complete true

# complete options instead of directory stack
zstyle ':completion:*' complete-options true

# case insensitive matching
zstyle ':completion:*' matcher-list 'm:{[:lower:][:upper:]}={[:upper:][:lower:]}'

# menu
zstyle ':completion:*' menu select

# colors
zstyle ':completion:*:default' list-colors "${(s.:.)LS_COLORS}"

# groups
zstyle ':completion:*' group-name ''
zstyle ':completion:*:*:-command-:*:*' group-order alias builtins functions commands
zstyle ':completion:*:*:*:*:corrections' format '%F{yellow}-- %d --%f'
zstyle ':completion:*:*:*:*:descriptions' format '%F{green}-- %d --%f'
zstyle ':completion:*:*:*:*:messages' format ' %F{purple} -- %d --%f'
zstyle ':completion:*:*:*:*:warnings' format ' %F{red}-- no matches found --%f'

# complete processes
zstyle ':completion:*:processes-names' command 'ps c -u ${USER} -o command | uniq'

# complete manuals by section
zstyle ':completion:*:manuals' separate-sections true
zstyle ':completion:*:manuals.*' insert-sections true

# vim keys in completion menu
bindkey -M menuselect h vi-backward-char
bindkey -M menuselect j vi-down-line-or-history
bindkey -M menuselect k vi-up-line-or-history
bindkey -M menuselect l vi-forward-char

### mappings

# proper searching
# bindkey -M vicmd '/' history-incremental-search-backward
# bindkey -M vicmd '?' history-incremental-search-forward
bindkey -M vicmd '/' fzf-history-widget

# disable <C-s> and <C-q>
stty stop undef
stty start undef

# edit the current command in vim with <C-e>
autoload -Uz edit-command-line
zle -N edit-command-line
bindkey '^e' edit-command-line

# new st window in current directory
# bindkey -s '^n' 'st 2> /dev/null & disown\n'

# use lf to switch directories with <C-o>
# lfcd() {
# 	temp=$(mktemp)
# 	lf -last-dir-path="$temp" "$@"
# 	if [[ -f $temp ]]; then
# 		dir=$(cat "$temp")
# 		rm -f "$temp" >/dev/null
# 		[[ -d $dir && $dir != $(pwd) ]] && cd "$dir"
# 	fi
# 	clear
# }
# bindkey -s '^o' 'lfcd\n'

### aliases

# ls
alias ls='eza --group --color=auto --group-directories-first'
alias la='ls -a'
alias l='ls -la'
alias ll='ls -l'
alias lt='ls -Ta'
alias llt='ls -T'

# color
alias ip='ip --color=auto'
alias diff='diff --color=auto'
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

# renames
alias v='nvim'
alias d='disown'
alias rust='evcxr'

# git
alias wt='git worktree'

# disowns
o()   { xdg-open  $@ >/dev/null 2>&1 & disown }
doc() { zathura   $@ >/dev/null 2>&1 & disown }
img() { nsxiv     $@ >/dev/null 2>&1 & disown }
vid() { celluloid $@ >/dev/null 2>&1 & disown }

# clipboard
alias clip='xclip -selection clipboard'

# dotfiles
alias dot="git --git-dir=${XDG_DATA_HOME:-"$HOME/.local/share"}/dotfiles --work-tree="$HOME""

# updates
alias upd='paru -Syyu --devel --noconfirm; paru -c --noconfirm; paru -Sc --noconfirm'
alias updm='sudo reflector --protocol http,https --country Germany --latest 20 --sort rate --save /etc/pacman.d/mirrorlist --verbose'
alias updmslow='updm --connection-timeout 20 --download-timeout 20'

alias todo="nvim $HOME/todo.md"

# calendar
alias cal='cal -m'

# suckless
alias i='sudo make install; make clean'

# latex
alias newdoc="cp $HOME/templ/latex/doc/main.tex ."
alias newpres="cp $HOME/templ/latex/pres/main.tex ."

# rust
alias c='cargo'

# ocaml
source '/home/weisbrja/.opam/opam-init/init.zsh'

# python
alias py='python'
venv() {
	local env
	if [ -z "$1" ]; then
		env='.venv'
	else
		env="${XDG_DATA_HOME:-"$HOME/.local/share"}/venvs/$1"
	fi
	if [ ! -d "$env" ]; then
		echo -n "venv not initialized.\ndo you want to create it? (yes/No) "
		read answer
		case "$answer" in
			y|yes) python -m venv "$env" ;;
			*) return 1 ;;
		esac
	fi

	source "$env/bin/activate"
}

# download audio only
alias yt-dlp-audio='yt-dlp --format ba --extract-audio --audio-format mp3'

# pferd
alias p="pferd --config $HOME/kit/other/pferd.cfg"

# pacdiff
alias pacdiff="sudo DIFFPROG='nvim -d' pacdiff"

# calibre alias because i always forget this name
alias books=calibre

### zellij
# export ZELLIJ_AUTO_EXIT='false'
# eval "$(zellij setup --generate-auto-start zsh)"

### start tmux
if [ -z "$TMUX" ]; then
	tmux && exit
fi
